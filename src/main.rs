#[macro_use] extern crate log;
#[macro_use] extern crate serde_json;
extern crate actix_web;
extern crate actix_files;
extern crate actix_rt;
extern crate handlebars;
extern crate serde;

use std::path::{Path, PathBuf};
use std::io;
use std::sync::Arc;
use std::fs;
use std::cmp;

use actix_web::{web, App, HttpResponse, HttpServer};
use actix_web::middleware::Logger;
use actix_web::get;
use handlebars::Handlebars;
use serde::{Serialize, Deserialize};

pub static ENTRY_DIR: &'static str = "entries";
pub static BIND_PORT: &'static str = "127.0.0.1:8000";

#[actix_rt::main]
async fn main() -> io::Result<()> {
    let mut handlebars = Handlebars::new();
    handlebars
        .register_templates_directory(".html", "./templates")
        .unwrap();
    let handlebars_ref = web::Data::new(Arc::new(handlebars));

    env_logger::init();

    HttpServer::new(move || {
        App::new()
            .wrap(Logger::default())
            .app_data(handlebars_ref.clone())
            .service(actix_files::Files::new("/assets", "assets")
                .show_files_listing()
                .use_last_modified(true))
            .service(index)
            .service(get_post)
            .service(search)
    }).bind(BIND_PORT)?.run().await
}

#[get("/post/{date}")]
async fn get_post(date: web::Path<(String,)>, hb: web::Data<Arc<Handlebars<'_>>>) -> HttpResponse {
    match fs::read_to_string(get_path(date.0.clone())) {
        Ok(contents) => {
            let html = comrak::markdown_to_html(&contents, &comrak::ComrakOptions::default());
            let data = json!({
                "body": html,
                "title": date.0.clone()
            });
            let body = hb.render("entry", &data).unwrap();
            HttpResponse::Ok()
                .content_type("text/html")
                .body(body)
        },
        Err(e) => HttpResponse::NotFound().body(e.to_string())
    }
}

#[get("/")]
async fn index(hb: web::Data<Arc<Handlebars<'_>>>) -> HttpResponse {
    let data = json!({
        "posts": get_entry_filenames()
    });
    let body = hb.render("index", &data).unwrap();

    HttpResponse::Ok().body(body)
}

#[derive(Deserialize)]
struct Search {
    query: String
}

#[derive(Serialize)]
struct Entry {
    title: String,
    left_text: String,
    right_text: String
}

#[get("/search")]
async fn search(s: web::Query<Search>, hb: web::Data<Arc<Handlebars<'_>>>) -> HttpResponse {
    let entries = get_related_entries(s.query.clone());
    let data = json!({
        "query": s.query.clone(),
        "entries": entries,
        "num_entries": entries.len()
    });
    let body = hb.render("search", &data).unwrap();

    HttpResponse::Ok().body(body)
}

fn get_related_entries(q: String) -> Vec<Entry> {
    let mut entries: Vec<Entry> = vec![];

    for p in get_entry_files() {
        let title = get_date_from_path(&p);
        match fs::read_to_string(&p) {
            Ok(contents) => entries.extend(get_entries_from_contents(q.clone(), contents, title)),
            Err(e) => debug!("{:?}", e)
        }
    }

    entries
}

fn get_date_from_path(p: &PathBuf) -> String {
    String::from(p.file_stem().unwrap().to_str().unwrap())
}

fn get_entries_from_contents(q: String, contents: String, title: String) -> Vec<Entry> {
    let mut entries: Vec<Entry> = vec![];
    const LEFT_OFFSET: isize = 200;
    const RIGTH_OFFSET: isize = 50;

    for (i, _) in contents.match_indices(&q) {
        let left = cmp::max(i as isize - LEFT_OFFSET, 0) as usize;
        let right = cmp::min(i as isize + RIGTH_OFFSET, contents.len() as isize) as usize;

        entries.push(Entry {
            title: title.clone(),
            left_text: String::from(&contents[left..i]),
            right_text: String::from(&contents[i+q.len()..right])
        });
    }

    entries
}

fn get_path(date: String) -> PathBuf {
    Path::new(ENTRY_DIR).join(date + ".md")
}

fn get_entry_files() -> Vec<PathBuf> {
    fs::read_dir(ENTRY_DIR).unwrap()
        .map(|r| r.unwrap().path())
        .collect()
}

fn get_entry_filenames() -> Vec<String> {
    let mut filenames = fs::read_dir(ENTRY_DIR).unwrap()
        .map(|r| {
            String::from(
                r
                .unwrap()
                .file_name()
                .into_string()
                .unwrap()
                .split_at(10)
                .0)
        })
    .collect::<Vec<String>>();

    filenames.sort();
    filenames
}
